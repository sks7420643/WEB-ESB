package ESB.Service.VFS;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

public class LogVFSService {

    private static final Logger logger = LoggerFactory.getLogger(LogVFSService.class);

    public static void doSomething(File file) {
        logger.info("Folder has new file: " + file.getName());
    }
}
