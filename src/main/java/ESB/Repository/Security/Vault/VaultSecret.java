package ESB.Repository.Security.Vault;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "vault")
public class VaultSecret {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Getter
    @Setter
    @Column(name = "key", unique = true)
    private String key;
    @Getter
    @Setter
    @Column(name = "value")
    private String value;

    public VaultSecret() {}
}
