package ESB.Repository.MSAD;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface MSADRepository extends JpaRepository<Employee, Long> {
    List<Employee> findByCanonicalName(String name);
}
