package ESB.Configuration;

import ESB.Service.Security.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

    @Autowired
    private AuthService authService;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
                .csrf(AbstractHttpConfigurer::disable)
//                .requiresChannel(channel ->
//                        channel.anyRequest().requiresSecure())
                .authorizeHttpRequests((authorize) -> authorize
                        .requestMatchers("/MSAD/**").hasAnyAuthority(Role.ADMIN.name, Role.MSAD.name)
                        .requestMatchers("/RoleSystem/**").hasAuthority(Role.ADMIN.name)
                        .requestMatchers("/Vault/**").hasAuthority(Role.ADMIN.name)
                        .anyRequest().permitAll())
                .userDetailsService(authService)
                .httpBasic(Customizer.withDefaults());
        return http.build();
    }

}
