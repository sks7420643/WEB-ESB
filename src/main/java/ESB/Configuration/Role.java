package ESB.Configuration;

public enum Role {
    ADMIN("Admin"),
    MSAD("MSADUser"),
    USER("User");

    public final String name;

    private Role(String name) {
        this.name = name;
    }
}
