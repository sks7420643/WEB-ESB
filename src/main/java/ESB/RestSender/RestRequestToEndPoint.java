package ESB.RestSender;

import ESB.Configuration.TrustStore;
import org.apache.http.ssl.SSLContextBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.net.ssl.SSLContext;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.Base64;

@Component
public abstract class RestRequestToEndPoint {

    protected String port;

    protected String address;

    protected String protocol;

    protected String method;

    protected String message;

    protected HttpClient client;

    protected HttpRequest.Builder httpBuilder;

    @Autowired
    protected TrustStore trustStore;

    public RestRequestToEndPoint(String port, String address, String protocol, String method, String message)
            throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException, KeyManagementException {
        this.port = port;
        this.address = address;
        this.protocol = protocol;
        this.method = method;
        this.message = message;
        this.httpBuilder = HttpRequest.newBuilder();
        httpBuilder.uri(URI.create(makeAddressToCall()));
//        String key = new File("").getAbsolutePath();
//        System.out.println(key);
//        String key1 = key.concat(trustStore.getStore());
//        System.out.println(key1);
//        SSLContext sslContext = new SSLContextBuilder()
//                .loadTrustMaterial(new File(key1), trustStore.getPassword().toCharArray())
//                .build();
//        this.client = HttpClient.newBuilder()
//                .sslContext(sslContext)
//                .build();
    }

    protected void addBasicAuth(String user, String password) {
        String valueToEncode = user + ":" + password;
        String headerValue = "Basic " + Base64.getEncoder().encodeToString(valueToEncode.getBytes());
        httpBuilder.header("Authorization", headerValue);
    }

    protected String makeAddressToCall() {
        return protocol + "://" + address + ":" + port + "/" + method;
    }

    public abstract String makeRequest();
}
