package ESB.Controller.VFS.Adapters;

import ESB.Controller.VFS.FileEvent;
import ESB.Controller.VFS.VFSListener;
import ESB.Service.VFS.LogVFSService;

public class VFSLogFileAdapter implements VFSListener {
    @Override
    public void onCreated(FileEvent event) {
        LogVFSService.doSomething(event.getFile());
    }

}
