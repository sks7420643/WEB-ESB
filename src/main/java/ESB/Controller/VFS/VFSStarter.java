package ESB.Controller.VFS;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Properties;

public class VFSStarter {

    private static final String PROPERTY_FILE_PATH = new File("").getAbsolutePath()
            .concat("/src/main/resources/vfs.properties");

    private static final Logger logger = LoggerFactory.getLogger(VFSStarter.class);

    public static void initVFSListeners(){
        try {
            Properties vfsProps = new Properties();
            vfsProps.load(new FileInputStream(PROPERTY_FILE_PATH));
            for (Object key : vfsProps.keySet()) {
                String[] value = vfsProps.getProperty((String) key).split("~");
                String folderName = value[0];
                String adapterName = value[1];
                File folder = new File(folderName);
                VFSWatcher watcher = new VFSWatcher(folder);
                Class<?> adapterClass = Class.forName(adapterName);
                Constructor<?> adapterConstructor = adapterClass.getConstructor();
                Object adapter = adapterConstructor.newInstance();
                watcher.getListeners().add((VFSListener) adapter);
                watcher.watch();
                logger.info("VFS listener to folder " + folderName + " is created");
            }

        } catch (ClassNotFoundException | InvocationTargetException | NoSuchMethodException | InstantiationException |
                 IllegalAccessException | IOException exception) {
            logger.error("Cant find vfs property file: " + exception.getMessage());
        }
    }
}
