package ESB.Controller.MSAD;

import ESB.Repository.MSAD.Employee;
import ESB.Service.MSAD.MSADService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/MSAD")
public class MSADController {

    @Autowired
    private MSADService service;

    @GetMapping("/LoadObjectsToDB")
    String loadToDB(){
        return service.loadUsersToDB();
    }

    @GetMapping("/getBySAMAccountName/{name}")
    List<Employee> findEmployee(@PathVariable("name") String name) {
        return service.findEmployeeBySAMAccountName(name);
    }

    @GetMapping("/Hi")
    String sayHi(){
        return "Hi";
    }
}
